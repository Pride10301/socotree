/**
 * @format
 */
import { UIManager, Platform } from 'react-native'
import { Navigation } from 'react-native-navigation'
import { registerScreens } from '@screens'

registerScreens()

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            component: {
                name: 'App'
            }
        },
    })
})

if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental(true)
}

// Listen for componentDidAppear screen events
Navigation.events().registerComponentDidAppearListener(({ componentId, componentName }) => {
    console.log(componentName)
})

console.disableYellowBox = true