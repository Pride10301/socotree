import { Navigation } from 'react-native-navigation'

export function registerScreens() {
    Navigation.registerComponent('index', () => require('../index').default)
    Navigation.registerComponent('App', () => require('../App').default)
    Navigation.registerComponent('DiscoverScreen', () => require('@screens/Discover/DiscoverScreen').default)
    Navigation.registerComponent('IntroductionScreen', () => require('@screens/Introduction/IntroductionScreen').default)
}