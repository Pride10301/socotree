import { Navigation } from 'react-native-navigation'

export const guestRoot = () => {
    Navigation.setRoot({
        root: {
            stack: {
                id: 'introRoot',
                children: [{
                    component: {
                        name: 'IntroductionScreen'
                    },
                }]
            }
        }
    })
}
export const userRoute = () => {
    Navigation.setRoot({
        root: {
            stack: {
                id: 'userRoot',
                children: [{
                    component: {
                        name: 'DiscoverScreen'
                    },
                }]
            }
        }
    })
}