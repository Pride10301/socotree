import React from 'react'
import { StyleSheet, View, FlatList, ScrollView, SafeAreaView, Text } from 'react-native'
import colors from '@constants/colors'
import FriendComponent from '@components/ItemContainer/FriendComponent'
import CircleComponent from '@components/BasicContainer/CircleComponent'
import ButtonStyle from '@components/DesignContainer/ButtonStyle'
import data from '@constants/data'

const DiscoverScreen = () => {

    const onPressButton = () => {
        console.log("onPressData")
    }

    const renderNewFriend = ({item, index}) => {
        return (
            <FriendComponent data={item} index={index+1} />
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollContainer}>
                <CircleComponent image={require('@assets/icons/ic_check/ic_check.png')}/>
                <Text style={styles.titleTextStyle}>We’ve found your circle!</Text>
                <Text style={styles.detailTextStyle}>After a strenious search we’ve found a circle for you! Two people with similar interests willing to connect now!</Text>
                <Text style={styles.subTitleTextStyle}>MEET YOUR NEW FRIENDS</Text>
                <FlatList
                    scrollEnabled={false}
                    style={styles.itemContainer}
                    data={data.friendData}
                    keyExtractor={(item, index) => String(index)}
                    renderItem={renderNewFriend} />
                <View style={styles.buttonContainer} >
                    <ButtonStyle title={"Go to Chat"} onPress={onPressButton} />
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}
DiscoverScreen.options = {
    topBar: {
        title: {
            text: 'CIRCLES',
            fontFamily: 'Rubik-Medium'
        },
        elevation: 0,
        noBorder: true,
    }
}

export default DiscoverScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.brandWhite,
    },
    scrollContainer: {
        flex:1,
    },
    titleTextStyle: {
        color: colors.brandBlack,
        fontSize: 50,
        fontFamily: "Rubik-Bold",
        paddingRight:15,
        paddingLeft:15,
        marginBottom:20
    },
    detailTextStyle: {
        color: colors.brandBlack,
        fontSize: 18,
        fontFamily: "Rubik-Medium",
        paddingRight:15,
        paddingLeft:15,
        marginBottom:110
    },
    subTitleTextStyle: {
        color: colors.brandBlack,
        fontSize: 10,
        fontFamily: "Rubik-Medium",
        paddingRight:15,
        paddingLeft:15,
    },
    itemContainer: {
        flex:1,
        marginTop:5,
        marginBottom:5
    },
    buttonContainer: {
        padding:15
    }
})