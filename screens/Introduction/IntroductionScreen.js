import React from 'react'
import { StyleSheet, View } from 'react-native'
import colors from '@constants/colors'

const IntroductionScreen = () => {
    return (
        <View style={styles.container} />
    );
}

export default IntroductionScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.brandWhite
    }
})