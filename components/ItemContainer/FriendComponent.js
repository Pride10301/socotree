import React from 'react'
import { StyleSheet, Text, View, Image, FlatList } from 'react-native'
import FastImage from 'react-native-fast-image'
import colors from '@constants/colors'
import variables from '@constants/variables'

const FriendComponent = ({ data, index }) => {
    const renderInterest = ({item, index}) => {
        return (
            <View style={[interestStyles.container,{marginTop: index < 3 ? 0 : 10}]}>
                <Text style={interestStyles.textStyle}>{item}</Text>
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <FastImage source={data.image} resizeMode={FastImage.resizeMode.cover} style={styles.profileImageStyle} />
            <View style={styles.infoContainer}>
                <View style={styles.indexContainer}>
                    <Text style={styles.indexTextStyle}>{index}</Text>
                </View>
                <View style={styles.profileInfoContainer}>
                    <Text style={styles.nameTextStyle}>{data.name}</Text>
                    <Text style={styles.ageTextStyle}>{data.age} years old</Text>
                </View>
            </View>
            <FlatList
                scrollEnabled={false}
                numColumns={3}
                showsHorizontalScrollIndicator={false}
                style={styles.interestContainer}
                data={data.interest}
                keyExtractor={(item, index) => String(index)}
                renderItem={renderInterest} />
        </View>
    )
}

export default FriendComponent

const interestStyles = StyleSheet.create({
    container: {
        width: "auto",
        height:35,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: colors.brandBlue,
        borderRadius:12,
        marginRight:10,
        paddingLeft:10,
        paddingRight:10,
    },
    textStyle: {
        fontFamily: "Rubik-Medium",
        fontSize: 15,
        color: colors.brandWhite
    }
})

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "auto",
        alignItems: "center",
        paddingTop:10,
        paddingLeft:15,
        paddingRight:15,
        paddingBottom:30
    },
    profileImageStyle: {
        width:"100%",
        height: (variables.deviceWidth-30)/1.5,
        borderRadius:15,
    },
    infoContainer: {
        width:"100%",
        flexDirection:"row",
        paddingTop:20,
        paddingBottom:20
    },
    indexContainer: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: colors.brandBlack,
        marginRight:15,
        alignItems:"center",
        justifyContent:"center"
    },
    indexTextStyle: {
        fontFamily: "Rubik-Medium",
        fontSize: 25,
        color: colors.brandWhite
    },
    profileInfoContainer: {
        flex: 1,
        marginRight: 10,
        justifyContent:"space-around"
    },
    nameTextStyle: {
        fontFamily: "Rubik-Medium",
        fontSize: 22,
        color: colors.brandBlack
    },
    ageTextStyle: {
        fontFamily: "Rubik-Regular",
        fontSize: 14,
        color: colors.brandGray
    },
    interestContainer: {
        width:"100%",
        height:"auto"
    }
})