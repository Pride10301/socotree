import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import colors from '@constants/colors'

const ButtonStyle = ({title, onPress}) => {
    return (
        <TouchableOpacity onPress={() => onPress()} style={styles.container}>
            <Text style={styles.titleTextStyle}>{title}</Text>
         </TouchableOpacity>
    );
}

export default ButtonStyle;

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: 52,
        borderRadius: 12,
        backgroundColor: colors.brandGreen,
        justifyContent: "center",
        alignItems: "center"
    },
    titleTextStyle: {
        color: colors.brandWhite,
        fontFamily: "Rubik-Medium",
        marginLeft: 20,
        marginRight: 20,
        fontSize: 18
    }
})