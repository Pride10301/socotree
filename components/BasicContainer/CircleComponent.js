import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import FastImage from 'react-native-fast-image'
import colors from '@constants/colors'
import variables from '@constants/variables'

const CircleComponent = ({image}) => {
    return (
        <View style={styles.container}>
            <View style={styles.halfLeftCircleStyle}/>
            <View style={[styles.smallCircleStyle,{backgroundColor:colors.brandPurple}]}/>
            <View style={styles.coreCircleStyle}>
                <FastImage source={image} resizeMode={FastImage.resizeMode.contain} style={styles.circleImageStyle} />
            </View>
            <View style={[styles.smallCircleStyle,{backgroundColor:colors.brandBlue}]}/>
            <View style={styles.halfRightCircleStyle}/>
        </View>
    );
}

export default CircleComponent;

const styles = StyleSheet.create({
    container: {
        width: "100%",
        flexDirection:"row",
        justifyContent: "center",
        justifyContent:"space-between",
        paddingTop:75,
        paddingBottom:75,
        alignItems: "center"
    },
    smallCircleStyle: {
        height: variables.smallCircleWidth,
        width: variables.smallCircleWidth,
        borderRadius: variables.smallCircleWidth/2
    },
    halfLeftCircleStyle: {
        height: 0,
        width: 0,
        transform: [{rotate: '45deg'}],
        marginLeft:-variables.smallCircleWidth/2,
        borderWidth:variables.smallCircleWidth/2,
        borderRadius:variables.smallCircleWidth/2,

        borderTopColor: colors.brandLightBlue,

        borderRightColor: colors.brandLightBlue,

        borderLeftColor: colors.brandTransparent,

        borderBottomColor: colors.brandTransparent,
    },
    halfRightCircleStyle: {
        height: 0,
        width: 0,
        transform: [{rotate: '45deg'}],
        marginRight:-variables.smallCircleWidth/2,
        borderWidth:variables.smallCircleWidth/2,
        borderRadius:variables.smallCircleWidth/2,

        borderTopColor: colors.brandTransparent,

        borderRightColor: colors.brandTransparent,

        borderLeftColor: colors.brandYellow,

        borderBottomColor: colors.brandYellow,
    },
    coreCircleStyle: {
        alignItems: "center",
        justifyContent: "center",
        height: variables.coreCircleWidth,
        width: variables.coreCircleWidth,
        backgroundColor: colors.brandGreen,
        borderRadius: variables.coreCircleWidth/2.5,
    },
    circleImageStyle: {
        width: variables.coreCircleWidth/1.75,
        height: variables.coreCircleWidth/1.75
    },
})