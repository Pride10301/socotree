import React, { useEffect } from 'react'
import { StyleSheet, View } from 'react-native'
import SplashScreen from 'react-native-splash-screen'
import { userRoute, guestRoute } from '@screens/router'
import colors from '@constants/colors'


const App = () => {

    useEffect(() => {
        SplashScreen.hide()
        userRoute()
    }, [])

    return (
        <View style={styles.container} />
    )
}

export default App

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.brandWhite
    }
})