import { Platform, Dimensions } from "react-native"

const platform = Platform.OS

const deviceHeight = Dimensions.get("window").height
const deviceWidth = Dimensions.get("window").width
const smallCircleWidth = deviceWidth*0.15
const coreCircleWidth = deviceWidth*0.3

export default {
    platform,
    deviceWidth,
    deviceHeight,
    smallCircleWidth,
    coreCircleWidth
}