export default {
    brandWhite: '#FFFFFF',

    brandPurple: '#EA00C4',
    brandGreen: '#05EA00',
    brandBlack: '#010101',
    brandGray: 'rgba(0,0,0,0.45)',
    brandLightBlue: '#1AE3FF',
    brandBlue: '#0047FF',
    brandYellow: '#FCD116',
    brandTransparent: 'transparent',
}   